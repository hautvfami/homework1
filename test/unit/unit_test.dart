import 'dart:convert';

import 'package:flutter_1_techmaster/api_provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

const userData = {
  "data": {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  }
};

void main() {
  test("Testing the network call", () async{
    print("Unit test stated: ");
    //setup the test
    final apiProvider = ApiProvider();
    apiProvider.client = MockClient((request) async {
      final mapJson = {'id':123};
      return Response(json.encode(mapJson),200);
    });
    final item = await apiProvider.fetchPosts();
    expect(item.id, 123);
  });
}